# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:hydrogen
#     text_representation:
#       extension: .py
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown] slideshow={"slide_type": "slide"}
# # Dipole Analysis

# %% slideshow={"slide_type": "slide"}
import numpy as np

import pysim.plot_functions

# %% slideshow={"slide_type": "slide"}
multipoles = np.loadtxt("../multipole_data/multipoles")

# %% slideshow={"slide_type": "slide"}
time = multipoles[:, 1]

# %% slideshow={"slide_type": "slide"}
dipole = multipoles[:, 3:6]

# %% slideshow={"slide_type": "slide"}
for i in range(3):
    pysim.plot_functions.plot_dipole_spectrum(dipole, time, i)
