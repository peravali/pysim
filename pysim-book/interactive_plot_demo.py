# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:hydrogen
#     text_representation:
#       extension: .py
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
%matplotlib inline

# %%
from ipywidgets import interactive 


# %%
def f(slope, offset):
    plt.figure(2)
    x = np.linspace(-10, 10, num=1000)
    plt.plot(x, slope*x + offset)
    plt.ylim(-5, 5)
    plt.show()


# %%
f(1,1)

# %%
interactive_plot = interactive(f, slope =(-2.0,2.0), offset=(-3,3,0.5))

# %%
interactive_plot

# %%
interactive_plot2 = interactive(f, slope =(0,5.0), offset=(-5,0,0.5))

# %%
interactive_plot2


# %%
def g(slope1, slope2, offset1, offset2):
    plt.figure(2)
    x = np.linspace(-10, 10, num=1000)
    plt.plot(x, slope1*x + offset1, x, slope2*x + offset2)
    plt.ylim(-5, 5)
    plt.xlim(-10,10)
    plt.show()


# %%
g(1,2, 1,2)

# %%
interactive_plot = interactive(g, slope1 =(-2.0,2.0), slope2=(-2.0,2.0), offset1=(-3,3,0.5),offset2=(-3,3,0.5))

# %%
interactive_plot

# %%
# %load https://matplotlib.org/stable/_downloads/f1ae4d59ecd3898684380f6391e3c42c/contour_demo.py
"""
============
Contour Demo
============

Illustrate simple contour plotting, contours on an image with
a colorbar for the contours, and labelled contours.

See also the :doc:`contour image example
</gallery/images_contours_and_fields/contour_image>`.
"""

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

def mycontour(xpos,ypos):
    delta = 0.025
    x = np.arange(-3.0, 3.0, delta)
    y = np.arange(-2.0, 2.0, delta)
    X, Y = np.meshgrid(x, y)
    Z1 = np.exp(-X**2 - Y**2)
    Z2 = np.exp(-(X - xpos)**2 - (Y - ypos)**2)
    Z = (Z1 - Z2) * 2

#




###############################################################################
# Or you can use a colormap to specify the colors; the default
# colormap will be used for the contour lines

    fig, ax = plt.subplots()
    im = ax.imshow(Z, interpolation='bilinear', origin='lower',
               cmap=cm.gray, extent=(-3, 3, -2, 2))
    levels = np.arange(-1.2, 1.6, 0.2)
    CS = ax.contour(Z, levels, origin='lower', cmap='flag', extend='both',
                linewidths=2, extent=(-3, 3, -2, 2))

# Thicken the zero contour.
    zc = CS.collections[6]
    plt.setp(zc, linewidth=4)

    ax.clabel(CS, levels[1::2],  # label every second level
              inline=True, fmt='%1.1f', fontsize=14)

    # make a colorbar for the contour lines
    CB = fig.colorbar(CS, shrink=0.8)

    ax.set_title('Lines with colorbar')

    # We can still add a colorbar for the image, too.
    CBI = fig.colorbar(im, orientation='horizontal', shrink=0.8)

    # This makes the original colorbar look a bit out of place,
    # so let's improve its position.

    l, b, w, h = ax.get_position().bounds
    ll, bb, ww, hh = CB.ax.get_position().bounds
    CB.ax.set_position([ll, b + 0.1*h, ww, h*0.8])

    plt.show()

    #############################################################################
    #
    # .. admonition:: References
    #
    #    The use of the following functions, methods, classes and modules is shown
    #    in this example:
    #
    #    - `matplotlib.axes.Axes.contour` / `matplotlib.pyplot.contour`
    #    - `matplotlib.figure.Figure.colorbar` / `matplotlib.pyplot.colorbar`
    #    - `matplotlib.axes.Axes.clabel` / `matplotlib.pyplot.clabel`
    #    - `matplotlib.axes.Axes.get_position`
    #    - `matplotlib.axes.Axes.set_position`


# %%
mycontour(1,1)

# %%
mycontour(-2,-2)

# %%
interactive_plot = interactive(mycontour, xpos =(-3.0,3.0), ypos = (-2,2))

# %%
interactive_plot

# %%
