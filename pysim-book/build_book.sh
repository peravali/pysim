#!/bin/bash

toplevel=$(git rev-parse --show-toplevel)

pushd $toplevel
  pushd pysim-book
    for pysrc in $(ls *.py); do
      jupytext --sync $pysrc
    done
    # cleanup old build artifacts
    rm -rf _build ../public
    cp -a ../README.md intro.md
    cp -a ../INSTALL.md pysim_installation.md
  popd
 # build the jupyter book
 jupyter-book build pysim-book
 # move the build artifacts to the public folder
 rm -rf public
 mv pysim-book/_build/html public
 rsync -av pysim-book/images/ public/images
 chmod -R ugo+rX public
popd
