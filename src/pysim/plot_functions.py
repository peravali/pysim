#!/usr/bin/env python3

import numpy as np
import pylab as plt
from scipy.fft import fft, fftfreq


def plot_dipole_spectrum(dipole, time, direction=0):
    """this function computes the FFT of the time-series signal of the
    electronic dipole"""
    if direction > 2:
        raise ValueError
    N = len(time)
    yf = fft(dipole[:, direction])
    timestep = time[1] - time[0]
    xf = fftfreq(N, 1 / timestep)
    plt.plot(xf, np.abs(yf))
    plt.show()
