import pytest

import numpy as np

from pysim.plot_functions import plot_dipole_spectrum

__author__ = "Antonia Freibert"
__copyright__ = "Antonia Freibert"
__license__ = "GPL-3.0-only"


def test_plot_dipole_spectrum():
    """API Tests"""
    dipole = np.zeros(10)
    time = np.zeros(10)
    with pytest.raises(ValueError):
        plot_dipole_spectrum(dipole, time, 10)

